package com.xiaolei.openokjoke.JNI

/**
 * Created by xiaolei on 2018/3/16.
 */

object JokeLib
{
    init
    {
        System.loadLibrary("joke-lib")
    }
    
    external fun getWXAPPID(): String
    external fun getWXAPPKEY(): String
    
    external fun getQQAPPID(): String
    external fun getQQAPPKEY(): String
    
    external fun getServerAddress():String
    
}
